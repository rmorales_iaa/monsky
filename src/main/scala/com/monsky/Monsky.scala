/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/Feb/2020
 * Time:  00h:44m
 * Description: None
 */
//=============================================================================
package com.monsky
//=============================================================================
//=============================================================================
import com.common.util.time.Time
import com.monsky.commandLine.CommandLineParser
import com.common.logger.MyLogger
import com.common.util.string.MyString
import com.common.util.time.Time._
import com.monsky.centroid.Centroid
//=============================================================================
import ch.qos.logback.classic.{Level, LoggerContext}
import org.slf4j.LoggerFactory
import java.time.{Instant, LocalDate}
//=============================================================================
//=============================================================================
object Monsky {
  //---------------------------------------------------------------------------
  val COMMAND_INFO_LEFT_SIDE_SIZE = 50
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
import Monsky._
case class Monsky() extends App with MyLogger {
  //---------------------------------------------------------------------------
  private var lastCommand = ""
  //---------------------------------------------------------------------------
  private def initialActions(): Unit = {

    // set time zone to UTC
    Instant.now.atZone(zoneID_UTC)

    //disable reactor logging up to level error
    LoggerFactory.getILoggerFactory().asInstanceOf[LoggerContext].getLogger("reactor.util").setLevel(Level.ERROR)

    //disable mongo logging up to level error
    LoggerFactory.getILoggerFactory().asInstanceOf[LoggerContext].getLogger("org.mongodb.driver").setLevel(Level.ERROR)
  }
  //---------------------------------------------------------------------------
  private def finalActions(startMs: Long): Unit = {
    warning(s"Monksy elapsed time: ${Time.getFormattedElapseTimeFromStart(startMs)}")
  }
  //---------------------------------------------------------------------------
  private def commandVersion(): Boolean = {
    lastCommand = CommandLineParser.COMMAND_VERSION
    println(Version.value)
    true
  }

  //---------------------------------------------------------------------------
  private def commandCentroid(cl: CommandLineParser): Boolean = {
    lastCommand = CommandLineParser.COMMAND_CENTROID

    info(s"${MyString.rightPad("\tinput directory", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${cl.centroid()}")
    info(s"${MyString.rightPad("\toutput csv file:", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${cl.csv()}")

    Centroid(cl.centroid(), cl.csv())
    true
  }
  //---------------------------------------------------------------------------
  def commandManager(cl: CommandLineParser): Boolean = {
    //version
    if (cl.version.isDefined) return commandVersion()
    if (cl.centroid.isDefined) return commandCentroid(cl)
    true
  }
  //---------------------------------------------------------------------------
  def run(cl: CommandLineParser): Unit = {
    val startMs = System.currentTimeMillis

    initialActions()

    commandManager(cl)

    finalActions(startMs)
  }
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Claret.scala
//=============================================================================
