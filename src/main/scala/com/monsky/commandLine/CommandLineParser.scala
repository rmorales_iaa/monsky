/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Jun/2023
 * Time:  20h:40m
 * Description: None
 */
package com.monsky.commandLine
//=============================================================================
import com.common.logger.MyLogger
import com.monsky.Version
import org.rogach.scallop._
import sun.security.util.KeyUtil.validate
//=============================================================================
import com.common.util.file.MyFile._
import com.common.util.path.Path._
//=============================================================================
//=============================================================================
object CommandLineParser {
  //---------------------------------------------------------------------------
  final val COMMAND_VERSION                    = "version"
  final val COMMAND_CENTROID                   = "centroid"
  //---------------------------------------------------------------------------
  final val VALID_COMMAND_SEQ = Seq(
      COMMAND_VERSION
    , COMMAND_CENTROID
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
import CommandLineParser._
class CommandLineParser(args: Array[String]) extends ScallopConf(args) with MyLogger {
  version(Version.value + "\n")
  banner(s"""monsky syntax
            |monsky [configurationFile][command][parameters]
            |[commnads and parameters]
            |\t[$COMMAND_VERSION]
            |\t[$COMMAND_CENTROID]
            | #---------------------------------------------------------------------------------------------------------
            |Example 0: Show version
            |  java -jar monsky.jar --$COMMAND_VERSION
            | #---------------------------------------------------------------------------------------------------------
            |Example 0: Calculate the centroids of all FITS images of directory 'd' and generate a csv 'csv' as result
            |  java -jar monsky.jar --$COMMAND_CENTROID d --csv csv
            | #---------------------------------------------------------------------------------------------------------
            |Options:
            |""".stripMargin)
  footer("\nFor detailed information, please consult the documentation!")
  //---------------------------------------------------------------------------
  val version = opt[String](
      required = false
    , noshort = true
    , descr = "Program version\n"
  )
  //---------------------------------------------------------------------------
  val centroid = opt[String](
      required = false
    , noshort = true
    , descr = "Calculate the centroid of a sequence of images and generate a csv with the results\n"
  )
  //---------------------------------------------------------------------------
  val csv = opt[String](
     required = false
    , noshort = true
    , descr = "Name of a output CSV file\n"
  )
  //---------------------------------------------------------------------------
  val commandSeq = Seq(version, centroid)
  mutuallyExclusive(commandSeq:_*)
  requireOne       (commandSeq:_*)
  verify()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CommandLineParser.scala
//=============================================================================
