/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  20/Jun/2023
 * Time:  14h:39m
 * Description: None
 */
package com.monsky.centroid
//=============================================================================
//=============================================================================
import com.common.configuration.MyConf
import com.common.csv.{CsvItem, CsvRow, CsvWrite}
import com.common.hardware.cpu.CPU
import com.common.image.mask.Mask
import com.common.logger.MyLogger
import com.common.util.path.Path
import com.common.image.myImage.MyImage
import com.common.util.parallelTask.ParallelTask
//=============================================================================
import scala.util.{Try,Success,Failure}
//=============================================================================
object Centroid extends MyLogger {
  //---------------------------------------------------------------------------
  private final val conf = MyConf(MyConf.c.getString("SourceDetection.conf"))

  private final val SIGMA_MULTIPLIER = conf.getDouble("SourceDetection.sigmaMultiplier")

  private final val ROUND_SOURCES        = conf.getInt("SourceDetection.roundSources")
  private final val SOURCE_MIN_PIX_COUNT = conf.getInt("SourceDetection.sourceMinPixCount")
  private final val SOURCE_MAX_PIX_COUNT = conf.getInt("SourceDetection.sourceMaxPixCount")

  private final val IMAGE_MASK_PARENT_KEY =  "SourceDetection.mask"
  //---------------------------------------------------------------------------
  private final val CSV_COL_NAME_IMAGE             = "image"
  private final val CSV_COL_NAME_BACKGROUND        = "img_background"
  private final val CSV_COL_NAME_BACKGROUND_RMS    = "img_background_rms"
  private final val CSV_COL_NAME_SIGMA_MULTIPLIER  = "img_sigma_multiplier"
  private final val CSV_COL_NAME_NOISE_TIDE        = "img_noise_tide"
  private final val CSV_COL_NAME_ROUND_SOURCE      = "img_src_round"
  private final val CSV_COL_NAME_MIN_PIX_COUNT     = "img_src_min_pix"
  private final val CSV_COL_NAME_MAX_PIX_COUNT     = "img_src_max_pix"

  private final val CSV_COL_NAME_SOURCE_FOCUS_CENTROID_X = "src_focus_centroid_pix_x"
  private final val CSV_COL_NAME_SOURCE_FOCUS_CENTROID_Y = "src_focus_centroid_pix_y"

  private final val CSV_COL_NAME_SEQ = Seq(
    CSV_COL_NAME_IMAGE
    , CSV_COL_NAME_BACKGROUND
    , CSV_COL_NAME_BACKGROUND_RMS
    , CSV_COL_NAME_SIGMA_MULTIPLIER
    , CSV_COL_NAME_NOISE_TIDE
    , CSV_COL_NAME_ROUND_SOURCE
    , CSV_COL_NAME_MIN_PIX_COUNT
    , CSV_COL_NAME_MAX_PIX_COUNT
    , CSV_COL_NAME_SOURCE_FOCUS_CENTROID_X
    , CSV_COL_NAME_SOURCE_FOCUS_CENTROID_Y
  )
  //---------------------------------------------------------------------------
  private val headerRow = CsvRow(CSV_COL_NAME_SEQ)
  //---------------------------------------------------------------------------
  def processImage(fileName: String
                   , csvWrite: CsvWrite): Unit = {
    val img = MyImage(fileName)
    if (img == null) {
      error(s"Error loading image:'$fileName'")
      return
    }
    //detect sources
    val background = img.getBackground()
    val backgroundRMS = img.getBackgroundRMS()
    val noiseTide = background + (backgroundRMS * SIGMA_MULTIPLIER)
    val maskSeq = Mask.parseMask(conf
      , IMAGE_MASK_PARENT_KEY
      , img.getDimension())

    val region = img.findSourceSeq(noiseTide, maskSeq, Some((SOURCE_MIN_PIX_COUNT, SOURCE_MAX_PIX_COUNT)))
    val sortedSourceSeq = region.toSegment2D_seq().sortWith(_.getFlux() > _.getFlux())

    if (!sortedSourceSeq.isEmpty) {
      val focus = sortedSourceSeq.head
      val focusCentroid = focus.getCentroid()

      //add to csv
      val csvRow = CsvRow()
      csvRow + CsvItem(CSV_COL_NAME_IMAGE, fileName)
      csvRow + CsvItem(CSV_COL_NAME_BACKGROUND, s"${f"$background%.1f"}")
      csvRow + CsvItem(CSV_COL_NAME_BACKGROUND_RMS, s"${f"$backgroundRMS%.1f"}")
      csvRow + CsvItem(CSV_COL_NAME_SIGMA_MULTIPLIER, SIGMA_MULTIPLIER)
      csvRow + CsvItem(CSV_COL_NAME_NOISE_TIDE, s"${f"$noiseTide%.1f"}")
      csvRow + CsvItem(CSV_COL_NAME_ROUND_SOURCE, ROUND_SOURCES)
      csvRow + CsvItem(CSV_COL_NAME_MIN_PIX_COUNT, SOURCE_MIN_PIX_COUNT)
      csvRow + CsvItem(CSV_COL_NAME_MAX_PIX_COUNT, SOURCE_MAX_PIX_COUNT)
      csvRow + CsvItem(CSV_COL_NAME_SOURCE_FOCUS_CENTROID_X, s"${f"${focusCentroid.x}%.3f"}")
      csvRow + CsvItem(CSV_COL_NAME_SOURCE_FOCUS_CENTROID_Y, s"${f"${focusCentroid.y}%.3f"}")
      synchronized(csvWrite.append(csvRow))
    }
  }
  //---------------------------------------------------------------------------
  private class ProcessImageDir(fileNameSeq: Array[String]
                               , threadCount: Int
                               , csvWrite: CsvWrite) extends ParallelTask[String](
    fileNameSeq
    , threadCount
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100
    , verbose = false) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(fileName: String) = {
      Try {
        processImage(fileName, csvWrite)
      }
      match {
        case Success(_) =>
        case Failure(e) =>
          error(s"Error processing file:'$fileName'")
          error(e.toString)
          error(e.getStackTrace.mkString("\n"))
      }
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import  Centroid._
case class Centroid(inputDir: String, csvFileName: String) {
  //---------------------------------------------------------------------------
  Path.ensureDirectoryExist(Path.getParentPath(csvFileName))
  val csvWrite = CsvWrite(csvFileName, headerRow)

  new ProcessImageDir(Path.getSortedFileListRecursive(inputDir, MyConf.c.getStringSeq("Common.fitsFileExtension")).map(_.getAbsolutePath).toArray
                     , threadCount = CPU.getCoreCount()
                     , csvWrite)
  csvWrite.close()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Centroid.scala
//=============================================================================