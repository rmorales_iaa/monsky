lazy val programName    = "monsky"
lazy val programVersion = "0.0.1"
lazy val authorList     = "Rafael Morales Muñoz (rmorales@iaa.es)"
lazy val license        = "This project is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)"
//-----------------------------------------------------------------------------
//common settings
//version will be calculated using git repository
lazy val commonSettings = Seq(
  name                 := programName
  , version            := programVersion
  , scalaVersion       := "2.12.10"
  , description        := "Monitoring sky"
  , organization       := "IAA-CSIC"
  , homepage           := Some(url("http://www.iaa.csic.es"))
)
//-----------------------------------------------------------------------------
//Main class
lazy val mainClassName = "com.monsky.Main"
//-----------------------------------------------------------------------------
//dependencies versions
val apacheCommonsCsv         = "1.8"
val apacheCommonsIO          = "2.11.0"
val apacheCommonsMath        = "3.6.1"
val apacheLogCoreVersion     = "2.19.0"
val apacheLogApiScalaVersion = "12.0"
val httpVersion              = "2.4.2"
val jamaVersion              = "1.0.3"
val javaxMailVersion         = "1.6.2"
val jSofaVersion             = "20210512"
val jsonParsingUpickleVersion = "2.0.0"
val mongoScalaDriverVersion  = "4.8.1"
val rTree2DVersion           = "0.11.2"
val scallopVersion           = "3.5.1"
val scalaReflectVersion      = "2.12.10"
val scalaTestVersion         = "3.2.0-M2"
val qosLogbackVersion        = "1.4.5"
val typeSafeVersion          = "1.4.2"
//-----------------------------------------------------------------------------
//external source code
val externalRootDirectory="/home/rafa/proyecto/common_scala/"
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "configuration")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "constant/astronomy")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "coordinate/spherical")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "csv")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "dataType")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "database/mongoDB")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "hardware/cpu")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "logger")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "image/astrometry")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "image/catalog")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "image/mask")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "image/mpo")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "image/myImage")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "image/estimator")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "image/filter")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "image/focusType")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "image/instrument")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "image/mpo")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "image/objectPosition")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "image/photometry")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "image/region")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "image/telescope")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "image/util")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "fits/metaData")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "fits/simpleFits")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "fits/synthetize")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "fits/util")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "fits/wcs")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "hardware/cpu")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "jpl")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "myJava/FitEllipse")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "myJava/gif")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "myJava/spice")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "myJava/zscale")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "geometry")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "math")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "plot/bmpImage")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "plot/maxima")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "stat")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "timeSeries")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "util/file")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "util/mail")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "util/native")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "util/path")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "util/parallelTask")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "util/pattern")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "util/string")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "util/time")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "util/parallelTask")
Compile / unmanagedSourceDirectories += file(externalRootDirectory + "util/util")

Compile / unmanagedSources / excludeFilter := {
  new SimpleFileFilter(f=> {
    val p = f.getCanonicalPath
    p.startsWith(externalRootDirectory + "database/mongoDB/spark/")
  })}
//-----------------------------------------------------------------------------
//resolvers
//sbt-plugin
resolvers += Resolver.sonatypeRepo("public")
//-----------------------------------------------------------------------------
//https://github.com/benblack86/finest
//To help improve code quality the following compiler flags will provide errors for deprecated and lint issues
scalacOptions ++= Seq(
  "-Xlint",
  "-deprecation",
  "-feature",
  "-unchecked"
  //  "-Xfatal-warnings"   //generate an error when warning raises. Enable when stable version was published
)
//-----------------------------------------------------------------------------
//assembly
assembly / mainClass := Some(mainClassName)
assembly / logLevel  := Level.Warn

assembly / assemblyMergeStrategy := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case _ => MergeStrategy.first
}
//-----------------------------------------------------------------------------
//tests
parallelExecution in test := false
//-----------------------------------------------------------------------------
//git versioning: https://github.com/sbt/sbt-git

git.useGitDescribe := true
git.uncommittedSignifier := None         //do not append suffix is some changes are not pushed to git repo
git.gitDescribePatterns := Seq("v*.*")   //format of the tag version

//https://stackoverflow.com/questions/56588228/how-to-get-git-gittagtoversionnumber-value
git.gitTagToVersionNumber := { tag: String =>
  if (tag.isEmpty)  None
  else Some(tag.split("-").toList.head)
}

//See configuration options in: https://github.com/sbt/sbt-buildinfo

//package and name of the scala code auto-generated
buildInfoPackage := "BuildInfo"
buildInfoObject := "BuildInfo"

//append build time
buildInfoOptions += BuildInfoOption.BuildTime

//append extra options to be generated
buildInfoKeys ++= Seq[BuildInfoKey](
  buildInfoBuildNumber
  , scalaVersion
  , sbtVersion
  , name
  , version
  , "authorList" -> authorList
  , "license" -> license
  , description
  , organization
  , homepage
  , BuildInfoKey.action("myGitCurrentBranch") {git.gitCurrentBranch.value}
  , BuildInfoKey.action("myGitHeadCommit")    {git.gitHeadCommit.value.get}
)
//-----------------------------------------------------------------------------
//output path for JNI C headers. Generate it with sbt javah
javah / target :=  sourceDirectory.value / "../native/header"
//-----------------------------------------------------------------------------
//dependencies list
lazy val dependenceList = Seq(

  //manage configuration files. https://github.com/typesafehub/config
  "com.typesafe" % "config" % typeSafeVersion

  //logging: https://logging.apache.org/log4j/2.x/manual/scala-api.html
  , "org.apache.logging.log4j" %% "log4j-api-scala" % apacheLogApiScalaVersion
  , "org.apache.logging.log4j" % "log4j-core" % apacheLogCoreVersion % Runtime
  , "ch.qos.logback" % "logback-classic" % qosLogbackVersion

  //csv management: https://mvnrepository.com/artifact/org.apache.commons/commons-csv
  , "org.apache.commons" % "commons-csv" % apacheCommonsCsv

  //https://mvnrepository.com/artifact/org.apache.commons/commons-math3 https://mvnrepository.com/artifact/org.apache.commons/commons-math3c
  , "org.apache.commons" % "commons-math3" % apacheCommonsMath

  //command line parser : https://github.com/scallop/scallop
  , "org.rogach" %% "scallop" % scallopVersion

  // https://mvnrepository.com/artifact/commons-io/commons-io
  , "commons-io" % "commons-io" % apacheCommonsIO

  //scala reflect
  , "org.scala-lang" % "scala-reflect" % scalaReflectVersion

  // basic linear algebra package for Java.  https://mvnrepository.com/artifact/gov.nist.math/jama
  , "gov.nist.math" % "jama" % jamaVersion

  //Java translation of the International Astronomical Union's C SOFA software library : http://javastro.github.io/jsofa/
  , "org.javastro" % "jsofa" % jSofaVersion

  //mongoDB
  , "org.mongodb.scala" %% "mongo-scala-driver" % mongoScalaDriverVersion

  //scala wrapper for HttpURLConnection. OAuth included. https://github.com/scalaj/scalaj-http
  , "org.scalaj" %% "scalaj-http" % httpVersion

  //json parsing: // https://mvnrepository.com/artifact/com.lihaoyi/upickle
  , "com.lihaoyi" %% "upickle" % jsonParsingUpickleVersion

  //R-tree on 2d: https://github.com/plokhotnyuk/rtree2d
  , "com.github.plokhotnyuk.rtree2d" %% "rtree2d-core" % rTree2DVersion

  //mail agent: https://javaee.github.io/javamail/
  , "com.sun.mail" % "javax.mail" % javaxMailVersion

  //scala test
  , "org.scalatest" %% "scalatest" % scalaTestVersion % Test
)
//=============================================================================
//root project
lazy val root = (project in file("."))
  .settings(commonSettings: _*)
  .enablePlugins(AssemblyPlugin, BuildInfoPlugin, GitVersioning)
  .settings(libraryDependencies ++= dependenceList)
//-----------------------------------------------------------------------------
//End of file 'build.sbt'
//-----------------------------------------------------------------------------
