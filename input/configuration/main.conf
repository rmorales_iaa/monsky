#==============================================================================
#Start of configuration file
#==============================================================================
AbsolutePath {
  #----------------------------------------------------------------------------
  base_local_path = "/home/rafa/proyecto/m2"
  #----------------------------------------------------------------------------
  nativeLibrary.path   = ${AbsolutePath.base_local_path}/native/
  #----------------------------------------------------------------------------
  jplSpice.library     = "JNISpice"
  #----------------------------------------------------------------------------
  cgal.library         = "cgal"
  #----------------------------------------------------------------------------
}
#==============================================================================
Background {
  //---------------------------------------------------------------------------
  //Sextractor is 5s faster than the algorithm implemented in m2 (with equivalent results)
  //Sextractor has been modified in file "makeit.c" to write out a file with those values
  // (background and background RMS)
  //This output file has the same name as the catalog filename but with the suffix ".deleteme"
  //--------------------------------------------------------------------------
  useSextactor = true  //it true use sextractor for background estimation. If false, then use the m2 own algorithm
  //--------------------------------------------------------------------------
  m2_Algorithm {
    //------------------------------------------------------------------------
    meshSizeX = 64
    meshSizeY = 64
    //------------------------------------------------------------------------
    sigmaClipping {
      iteration = 3
      scale     = 3
    }
    //------------------------------------------------------------------------
  }
  //--------------------------------------------------------------------------
}
#==============================================================================
SourceCentroid {
  //--------------------------------------------------------------------------
  //valid options: cntrd (daophot)
  //               gcntrd (daophot)
  //               centreOfMasses
  fallbackAlgorithmSeq  = ["cntrd", "gcntrd" ,"centreOfMasses"]
  //--------------------------------------------------------------------------
}
#==============================================================================
FWHM {  // full width at half maximum
  //---------------------------------------------------------------------------
  sources {
    //-------------------------------------------------------------------------
    psefx {
      scriptPath = "input/sextractor/"
      scriptName = "run"
    }
    //-------------------------------------------------------------------------
    //obtain the fwhm of a source using least square fit of a several types of 2D guassians: circular, elliptycal and mosfat
    //It dependes on python version 3.6 and the following python packages: numpy and scipy
    aspyLib {
      call = "python3 input/aspyLib/aspyLib.py"
    }
  }
  //-------------------------------------------------------------------------
  image { //average of the fwhm of the sources
    //valid options: psfex, sextractor, m2
    algorithm = "psfex"
    fallback  = "m2"
  }
  //---------------------------------------------------------------------------
  algorithm {
    //-------------------------------------------------------------------------
    m2 {
      sigmaScale  = 2.5
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
#==============================================================================
BellShapeFit {
  //---------------------------------------------------------------------------
  //0   => GAUSSIAN_2D_SIMPLE_FIT
  //1   => GAUSSIAN_2D_SIMPLE_CENTRAL_ROW_COL_FIT

  //2   => GAUSSIAN_2D_ELLIPTICAL_FIT
  //3   => GAUSSIAN_2D_ELLIPTICAL_ROTATED_FIT

  //10  => NORMAL_2D_ELLIPTICAL
  //11  => NORMAL_2D_ELLIPTICAL_ROTATED_FIT

  //20  => MOFFAT_ELLIPTICAL_2D_FIT
  //21  => MOFFAT_ELLIPTICAL_ROTATED_FIT

  //100 => ASPLIB_GAUSSIAN_2D_CIRCULAR_FIT
  //101 => ASPLIB_GAUSSIAN_2D_ELLIPTICAL_FIT
  //102 => ASPLIB_MOFFAT_2D_CIRCULAR_FIT
  //103 => ASPLIB_MOFFAT_2D_ELLIPTICAL_FIT

  fitAlgorithm =   21
  //---------------------------------------------------------------------------
  sampligRegionX = 0  //Extra pix size (in x direction) added to the surrounding source box to calcualte the fit
  sampligRegionY = 0  //Extra pix size (in y direction) added to the surrounding source box to calcualte the fit
  //---------------------------------------------------------------------------
}
#==============================================================================
SourceDetection {
  //---------------------------------------------------------------------------
  conf = "input/configuration/sourceDetection.conf"
  //---------------------------------------------------------------------------
}
#==============================================================================
Common {
  #----------------------------------------------------------------------------
  coreCount = 1
  //-1 indicates that all thread cores will be used
  //an integer number > 0 fix the number of cores to be used
  #----------------------------------------------------------------------------
  fitsFileExtension = [".fit", ".fts", ".fits"]
  #----------------------------------------------------------------------------
}
#==============================================================================
#End of configuration file
#==============================================================================